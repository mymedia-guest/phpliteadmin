<?php
// This is a little configuration file provided by the Debian maintainer.
// It contains minimal sensible defaults. For the full documentation see the
// phpLiteAdmin wiki on Bitbucket[1], or the sample configiguration from the
// upstream authors on [2].
//
//  [1]: https://bitbucket.org/phpliteadmin/public/wiki/Configuration
//  [2]: /usr/share/doc/phpliteadmin/examples/phpliteadmin.config.sample.php

// phpLiteAdmin asks for this passphrase before authorizing a user.
// It is recommended to change the default value for better security.
//$password = 'admin';

// The working directory where phpLiteAdmin searches databases. Please keep
// in mind that a user cannot change it within web-browser.
$directory = '/var/lib/phpliteadmin';

// The language used on pages within phpLiteAdmin web-application. Possible
// languages are: ar, cn, cz, de, en, esla, fr, gr, it, nl, pl, pt, ru, tw.
//$language = 'en';

// The default theme used by phpLiteAdmin. Additional themes are available in
// the phpliteadmin-themes package. Specify a CSS file name of a theme.
//$theme = 'phpliteadmin.css';
